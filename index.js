function countSums(X, nums, lastNumIndex) {
    console.log(X, nums.slice(0, lastNumIndex + 1))
    if (X<1 || lastNumIndex<0) {
        return 0;
    }
 
    const biggestNum = nums[lastNumIndex];
    let count = 0;
    if (X==biggestNum) {
        ++count;
    }
    --lastNumIndex;
    count += countSums(X - biggestNum, nums, lastNumIndex);
    count += countSums(X, nums, lastNumIndex);
    return count;
}

function solve(X, N) {
    const powNums = [];
    let i = 0, powI;
    while ((powI=Math.pow(++i, N)) <= X) {
        powNums.push(powI);
    }
    return countSums(X, powNums, powNums.length-1);
}

function processData(input) {
    const [X, N] = input.split('\n').map(x => parseInt(x, 10));
    const res = solve(X, N);
    console.log(res);
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
